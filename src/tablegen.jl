module tablegen

export createtable

using SpecialFunctions
using QuadGK
using Interpolations
# Synchrontron functions

F1 = x -> x * quadgk(y -> besselk(5/3, y), x, Inf, rtol=1E-9)[1]
F2 = x -> x * besselk(2/3, x)
function SynchrotronCgen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/χₑ)*(2/3)
        return χₑ/δ * F1(z)
    end
    return core
end

function SynchrotronQgen(χₑ::Float64)
    function core(δ::Float64)
        zq = (δ/(1-δ)/χₑ) * (2/3)
        return χₑ * (1-δ)/δ * (
            F1(zq) + 3/2 * δ*χₑ*zq*F2(zq) 
        )
    end
    return core
end

# Stochasticspin model
function Aζgen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        return δ^2/(1-δ) * (
        airyaiprime(z)/z + airyai(z)/sqrt(z)
        )
    end
    return core
end
function Bζgen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        return δ^2/(1-δ) * (
        airyaiprime(z)/z - airyai(z)/sqrt(z)
        )
    end
    return core
end

function Aηgen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        return δ^2/(1-δ) * (
        airyaiprime(z)/z
        )
    end
    return core
end

Ai₁ = x -> quadgk(airyai, x, Inf, rtol=1E-9)[1]

function Aκgen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)

        return δ^2/(1-δ) * (
        -Ai₁(z)
        )
    end
    return core
end

function ∫Ai₁Ai′gen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        g = 1 + δ^2/2/(1-δ)
        return Ai₁(z) + g*2*airyaiprime(z)/z
    end
    return core
end
function ∫δAigen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        return δ*airyai(z)/sqrt(z)
    end
    return core
end
# integrator of averaged radiation energy
function ∫δAi₁Ai′gen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        g = 1 + δ^2/2/(1-δ)
        return δ*(Ai₁(z) + g*2*airyaiprime(z)/z)
    end
    return core
end
function ∫δδAigen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        return δ*δ*airyai(z)/sqrt(z)
    end
    return core
end

# para = para1 + s*para2
function para1gen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        g = 1 + δ^2/2/(1-δ)
        return Ai₁(z) + g*2*airyaiprime(z)/z + δ/(1-δ)*airyai(z)/√z
    end
    return core
end
function para2gen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        g = 1 + δ^2/2/(1-δ)
        return +Ai₁(z) + 2*airyaiprime(z)/z + δ*airyai(z)/√z
    end
    return core
end
# anti = anti1 + s*anti2
function anti1gen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        g = 1 + δ^2/2/(1-δ)
        return Ai₁(z) + g*2*airyaiprime(z)/z - δ/(1-δ)*airyai(z)/√z
    end
    return core
end
function anti2gen(χₑ::Float64)
    function core(δ::Float64)
        z = (δ/(1-δ)/χₑ)^(2/3)
        g = 1 + δ^2/2/(1-δ)
        return -Ai₁(z) - 2*airyaiprime(z)/z + δ*airyai(z)/√z
    end
    return core
end

function createtable(logχ1::Real, logχ2::Real, N::Int)
    # 1d tables, total probability, integrate δ from 0 to 1
    gen1d = [
        :SynchrotronQgen, :SynchrotronCgen,
        :Aζgen, :Bζgen, :Aηgen, :Aκgen, 
        :∫Ai₁Ai′gen, :∫δAigen,
        :∫δAi₁Ai′gen, :∫δδAigen,
        :para1gen, :para2gen,
        :anti1gen, :anti2gen,
    ]
    tables1d = [
        :SynchrotronQs, :SynchrotronCs,
        :Aζs, :Bζs, :Aηs, :Aκs, 
        :∫Ai₁Ai′s, :∫δAis,
        :∫δAi₁Ai′s, :∫δδAis,
        :para1s, :para2s,
        :anti1s, :anti2s,
    ]
    interpolators1d = [
        :SynchrotronQ, :SynchrotronC,
        :Aζ, :Bζ, :Aη, :Aκ, 
        :∫Ai₁Ai′, :∫δAi,
        :∫δAi₁Ai′, :∫δδAi,
        :para1, :para2,
        :anti1, :anti2,
    ]
    
    # 2d tables
    gen2d = [
        # integrals of synchrotron function, total radiation probability
        :SynchrotronQgen, :SynchrotronCgen,
        # integrals of spin-dependent radiation, total radiation probability
        :∫Ai₁Ai′gen, :∫δAigen
    ]
    tables2d = [
        :∫ᵟSynchrotronQs, :∫ᵟSynchrotronCs,
        :∫ᵟAi₁Ai′s, :∫ᵟδAis,
    ]
    interpolators2d = [
        :∫ᵟSynchrotronQ, :∫ᵟSynchrotronC,
        :∫ᵟAi₁Ai′, :∫ᵟδAi,
    ]

    χₑs = (10).^LinRange(logχ1, logχ2, N)

    for (gen, tbl) in zip(gen1d, tables1d)
        println("generating $tbl")

        code = quote
            $tbl = zeros($N)
            Threads.@threads for i = 1:$N
                $tbl[i] = quadgk($gen($χₑs[i]), 0, 1)[1]
            end
        end
        eval(code)
    end
    
    # (δ, χₑ) tables, integral for wach δ
    logδ1 = -6
    logδ2 = log10(1-1E-6)
    
    δs = 10 .^ LinRange(logδ1, logδ2, N)
    dδ = diff(δs)
    
    for (gen, tbl) in zip(gen2d, tables2d)
        println("generating $tbl")

        code = quote
            $tbl = zeros($N, $N)
            Threads.@threads for i = 1:$N
                vals = $gen($χₑs[i]).($δs)
                $tbl[i, 1] = quadgk($gen($χₑs[i]), 0, $δs[1])[1]
                $tbl[i, 2:end] .= cumsum( (vals[1:end-1].+vals[2:end])/2 .* $dδ) .+ $tbl[i, 1]
            end
        end
        eval(code)
    end
    
    open("functions.jl", "w") do file
        # interpolation axes
        code = quote
            const χₑs = $χₑs
            const δs = $δs

            const logχ1 = $logχ1
            const logχ2 = $logχ2
            const logδ1 = $logδ1
            const logδ2 = $logδ2

            const Δlogχ = (logχ2 - logχ1) / ( $N-1 )
            const Δlogδ = (logδ2 - logδ1) / ( $N-1 )
        end
        write(file, string(code)*"\n")
        # tables
        for (tbl, interp) in zip(tables1d, interpolators1d)
            write(file, "const $tbl = " * string(eval(tbl)) * "\n")
        end
        for (tbl, interp) in zip(tables2d, interpolators2d)
            write(file, "const $tbl = " * string(eval(tbl)) * "\n")
        end

        # 1D interpolators
        for (tbl, interp) in zip(tables1d, interpolators1d)
            code = quote
                @inline function $interp(χ::Float64)
                    logχ = log10(χ)
                    if logχ < $logχ1
                        return 0.0
                    end
                    if logχ > logχ2
                        return $(tbl)[end]
                    end
                    i = floor(Int, (logχ - $logχ1) / Δlogχ) + 1
        
                    xl = χₑs[i]
                    xr = χₑs[i+1]
        
                    yl = $tbl[i]
                    yr = $tbl[i+1]
        
        
                    return yl + (yr - yl)/(xr - xl)*(χ - xl)
                end

            end
            write(file, string(code)*"\n")
        end

        # 2D interpolators
        for (tbl, interp) in zip(tables2d, interpolators2d)

            code = quote
                @inline function $interp(χ::Float64, δ::Float64)
                    logχ = log10(χ)
                    logδ = log10(δ)
                    
                    if logχ < logχ1
                        return 0.0
                    end
                    if logχ > logχ2
                        logχ = logχ2
                    end
                    @assert logδ1 <= logδ <= logδ2 "δ out of interpolation range"

                    i = floor(Int, (logχ - logχ1) / Δlogχ) + 1
                    j = floor(Int, (logδ - logδ1) / Δlogδ) + 1

                    if logχ == logχ2
                        i = $(N - 1)
                    end
                    if logδ == logδ2
                        j = $(N - 1)
                    end

                    χl = χₑs[i]
                    χr = χₑs[i+1]

                    δl = δs[j]
                    δr = δs[j+1]

                    zl = $tbl[i, j  ] + ($tbl[i+1, j  ] - $tbl[i, j  ])/(χr - χl) * (χ - χl)
                    zr = $tbl[i, j+1] + ($tbl[i+1, j+1] - $tbl[i, j+1])/(χr - χl) * (χ - χl)

                    return zl + (zr - zl)/(δr - δl) * (δ - δl)
                end
            end
            write(file, string(code)*"\n")
        end

    end # end file
end

end
