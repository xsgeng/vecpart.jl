export α,c,mₑ,e,ħ,aₑ,mₑ²,c²,q,m,cSI,λSI,λ,T,k,fs

const α = 0.0072973525693
# atomic units
const c = 1.0 / α
const mₑ = 1.0
const e = 1.0
const ħ = 1.0

const aₑ = 1.16E-3

const mₑ² = mₑ^2
const c² = c^2
const mₑ²c² = mₑ² * c²

const q = -e
const m = mₑ

const cSI = 2.99792458E8

const λSI = 0.8E-6
const λ = λSI / 5.2917721092E-11
const T = λ / c
const k = 2*pi / λ

const fsSI = 1E-15
const fs = 1E-15 / 2.418884326505E-17
