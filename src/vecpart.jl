module vecpart

using Printf
using LinearAlgebra
using SpecialFunctions
using QuadGK
using Interpolations

export main

include("utils.jl")
include("constants.jl")

include("lasers.jl")

include("functions.jl")
include("radiation.jl")


function main(; part::Particles, laser::Function, rad::Radiation = none, photon::Bool = false, diags::Vector{Function}=Function[], rr::Bool=true, verbose::Bool=true)
    npart = part.npart
    nt = part.nt
    dt = part.dt

    ts = (0:nt-1) * dt

    em = EM(npart)

    ndiag = length(diags)
    diag_res = Vector{Array{Float64,2}}(undef, ndiag)
    for idiag = 1:ndiag
        dummy_res = (rad != none) & (photon) ? diags[idiag](part, part) : diags[idiag](part)
        diag_res[idiag] = zeros(length(dummy_res), nt)
    end

    wb = waitbar(nt)
    for (it, t) in enumerate(ts)
        laser(em, part.r, t)

        pushparticle!(part, em)


        if rad != none
            dp = radiate(rad, part, em)
        end

        if (rad != none) & (photon)
            photons = createphotons(part.r, -dp)
        end

        if rr & (rad != none)
            part.p .+= dp
        end

        for idiag = 1:ndiag
            if (rad != none) & (photon)
                diag_res[idiag][:, it] .= diags[idiag](part, photons)
            else
                diag_res[idiag][:, it] .= diags[idiag](part)
            end
        end

        if verbose
            wb(it)
        end
    end
    if isempty(diag_res)
        return part
    end
    return tuple(diag_res..., part)
end

function pushparticle!(part::Particles, em::EM)
    dt = part.dt
    E = em.E
    B = em.B
    # Boris
    fac = (q * dt) / (2 * m)

    uⁿ = part.p ./ m
    # 电场1
    u⁻ = uⁿ + fac .* E
    # 磁场2
    γ⁻ = sqrt.(1 .+ u⁻ ⋅ u⁻ / c^2)
    T = fac * B ./ γ⁻
    S = 2 * T ./ (1 .+ T ⋅ T)
    # 旋转
    u⁺ = u⁻ + (u⁻ + (u⁻ × T)) × S
    # 电场3
    uⁿ⁺¹ = u⁺ + fac * E

    # T-BMT
    s = part.s
    β⁻ = u⁻ ./ γ⁻ ./ c
    Ωₛ =
        (aₑ .+ 1 ./ γ⁻) .* B*c - aₑ .* γ⁻ ./ (1 .+ γ⁻) .* (β⁻ ⋅ B*c) .* β⁻ -
        (aₑ .+ 1 ./ (1 .+ γ⁻)) .* (β⁻ × E)
    T = fac/c * Ωₛ
    S = 2 * T ./ (1 .+ T ⋅ T)
    # 旋转
    part.s .= s + (s + (s × T)) × S

    part.p .= m * uⁿ⁺¹
    part.γ .= get_gamma(part)
    part.v .= part.p ./ part.γ / m
    part.χ .= get_chi(part, em)

    part.r .+= (uⁿ⁺¹ + uⁿ) ./ (2 * γ⁻) * dt

end

end
