export none, LL, stochasticspin, cLL, synchrotronQ, gst
@enum Radiation begin
    none
    LL
    cLL
    synchrotronQ
    stochasticspin
    gst
end

function radiate(rad::Radiation, part::Particles, em::EM)
    if rad == LL
        return _LL(part, em)
    end
    if rad == cLL
        return _cLL(part, em)
    end
    if rad == synchrotronQ
        return _synchrotronQ(part, em)
    end
    if rad == stochasticspin
        return _stochasticspin(part, em)
    end
    if rad == gst
        return _gst(part, em)
    end
end

function _LL(part::Particles, em::EM)
    dt = part.dt
    γ = part.γ
    v = part.v

    E = em.E
    B = em.B

    Fᵣᵣ =
        -2 // 3 * q^4 * γ .^ 2 / (m^2 * c^5) .*
        (sum((E .+ v × B) .^ 2, dims = 1) .- (v ⋅ E / c) .^ 2) .* v

    return Fᵣᵣ * dt
end

function _cLL(part::Particles, em::EM)
    dt = part.dt
    γ = part.γ
    v = part.v
    χₑ = part.χ

    E = em.E
    B = em.B

    Fᵣᵣ =
        -2 // 3 * q^4 * γ .^ 2 / (m^2 * c^5) .*
        (sum((E .+ v × B) .^ 2, dims = 1) .- (v ⋅ E / c) .^ 2) .* v


    g = @. (1 + 4.8*(1+χₑ)*log(1+1.7*χₑ)+2.44*χₑ^2)^(-2/3)
    return g .* Fᵣᵣ * dt
end

function _synchrotronQ(part::Particles, em::EM)
    γ = part.γ
    v = part.v
    χₑ = part.χ
    dt = part.dt

    outoftable = χₑ[:] .> 10.0^logχ2
    if any( outoftable )
        @warn "χₑ greater than 100, set to 100"
        χₑ[1, outoftable] .= 100
    end
    qed = χₑ[:] .> 10.0^logχ1

    fac = sqrt(3)/2π * α * m * c^2/ħ ./ γ

    # total radiation prob
    Ṗ = SynchrotronQ.(χₑ) .* fac
    part.optical_depth[1, qed] .-= Ṗ[1, qed] * dt
    event = part.optical_depth[:] .< 0
    # reset optical_depth
    part.optical_depth[1, event] .= -log.(1 .- rand(sum(event)))
    # photon energy
    δ = zeros(1, part.npart)
    for ipart = findall(event)
        χₑⁱ = χₑ[ipart]

        ℙ¹ = ∫ᵟSynchrotronQ(χₑⁱ, δs[end])
        ℙᵟ = ∫ᵟSynchrotronQ.(χₑⁱ, δs)

        P = ℙᵟ./ℙ¹
        R = rand() * (P[end]-P[1]) + P[1]
        idx = findlast(R .> P)
        δ[1, ipart] = δs[idx] + (δs[idx+1]-δs[idx])/(P[idx+1]-P[idx]) * (R - P[idx])
    end

    return -part.p .*  δ
end

function _stochasticspin(part::Particles, em::EM)
    γ = part.γ
    v = part.v
    χₑ = part.χ
    dt = part.dt

    outoftable = χₑ[:] .> 100
    if any( outoftable )
        @warn "χₑ greater than 100, set to 100"
        χₑ[1, outoftable] .= 100
    end
    qed = χₑ[:] .> 1E-3

    E = em.E
    B = em.B

    B′ = γ.*(B .- v×E / c²) .- (γ .- 1) .* (v⋅B) .* v ./ (v⋅v);
    E′ = γ.*(E .+ v×B) .- (γ .- 1) .* (v⋅E) .* v ./ (v⋅v);
    k′ = E′×B′

    ζ = B′ ./ sqrt.(B′⋅B′)
    η = E′ ./ sqrt.(E′⋅E′)
    κ = k′ ./ sqrt.(k′⋅k′)

    sζ = part.s ⋅ ζ
    sη = part.s ⋅ η
    sκ = part.s ⋅ κ

    fac = -α * m * c^2/ħ ./ γ

    # total radiation prob
    Ṗ = (∫Ai₁Ai′.(χₑ) + sζ .* ∫δAi.(χₑ)) .* fac
    part.optical_depth[1, qed] .-= Ṗ[1, qed] * dt
    event = part.optical_depth[:] .< 0
    # reset optical_depth
    part.optical_depth[1, event] .= -log.(1 .- rand(sum(event)))
    # photon energy
    δ = zeros(1, part.npart)
    for ipart = findall(event)
        χₑⁱ = χₑ[ipart]
        sζⁱ = sζ[ipart]
        sηⁱ = sη[ipart]
        sκⁱ = sκ[ipart]

        ℙ¹ = ∫ᵟAi₁Ai′(χₑⁱ, δs[end]) + sζⁱ * ∫ᵟδAi(χₑⁱ, δs[end])
        ℙᵟ = ∫ᵟAi₁Ai′.(χₑⁱ, δs) .+ sζⁱ * ∫ᵟδAi.(χₑⁱ, δs)

        P = ℙᵟ./ℙ¹
        R = rand() * (P[end]-P[1]) + P[1]
        idx = findlast(R .> P)
        δ[1, ipart] = δs[idx] + (δs[idx+1]-δs[idx])/(P[idx+1]-P[idx]) * (R - P[idx])


        # spin flips
        Aζₑ = Aζ(χₑⁱ) * fac[ipart]/2
        Bζₑ = Bζ(χₑⁱ) * fac[ipart]/2
        Aηₑ = Aη(χₑⁱ) * fac[ipart]/2
        # Bηₑ = Aηₑ
        Aκₑ = Aκ(χₑⁱ) * fac[ipart]/2
        # Bκₑ = Aκₑ

        sᵀ = sqrt(sηⁱ^2 + sκⁱ^2)
        ṡᵀ = -2 * (Aηₑ * sηⁱ^2 + Aκₑ * sκⁱ^2 ) / sᵀ

        # always positive
        Δsᵀ = -ṡᵀ*dt / (Ṗ[ipart] * dt)

        P_jump = sᵀ > 0 ? (Δsᵀ / sᵀ) : 1.0
        jump = rand() < P_jump
        # @assert all(Ṗ[ipart] * dt .< 1) "P⋅dt = $(maximum(Ṗ[ipart] * dt)) > 1"
        if jump
            ṡζ = (Aζₑ - Bζₑ) - (Aζₑ + Bζₑ) * sζⁱ
            # scaled by the probability of jumps and radiation
            Δsζ = ṡζ*dt / (Ṗ[ipart] * dt) / P_jump

            para = rand() < (Δsζ + sζⁱ + 1)/2
            if para
                part.s[:, ipart] .= @views ζ[:, ipart]
            else
                part.s[:, ipart] .= @views -ζ[:, ipart]
            end

        end
    end


    return -part.p .*  δ
end

function _gst(part::Particles, em::EM)
    γ = part.γ
    v = part.v
    χₑ = part.χ
    dt = part.dt
    
    outoftable = χₑ[:] .> 100
    if any( outoftable )
        @warn "χₑ greater than 100, set to 100"
        χₑ[1, outoftable] .= 100
    end
    qed = χₑ[:] .> 1E-3
    
    E = em.E
    B = em.B
    
    B′ = γ.*(B .- v×E/c²) .- (γ .- 1) .* (v⋅B) .* v ./ (v⋅v);
    E′ = γ.*(E .+ v×B)    .- (γ .- 1) .* (v⋅E) .* v ./ (v⋅v);
    k′ = E′×B′
    
    ζ = B′ ./ sqrt.(B′⋅B′)
    η = E′ ./ sqrt.(E′⋅E′)
    κ = k′ ./ sqrt.(k′⋅k′)
    
    sζ = part.s ⋅ ζ
    sη = part.s ⋅ η
    sκ = part.s ⋅ κ
    
    fac = -α * m * c^2/ħ ./ γ
    
    # total radiation prob
    δ = @. (∫δAi₁Ai′(χₑ) + sζ * ∫δδAi(χₑ)) * fac * dt
    δ[.!qed] .= 0.0

    # spin flips
    Aζₑ = Aζ.(χₑ) .* fac/2
    Bζₑ = Bζ.(χₑ) .* fac/2
    Aηₑ = Aη.(χₑ) .* fac/2
    # Bηₑ = Aηₑ
    Aκₑ = Aκ.(χₑ) .* fac/2
    # Bκₑ = Aκₑ

    # B
    τ   = @. 1 / (Aζₑ + Bζₑ)
    sζ .= @. (Aζₑ - Bζₑ)/(Aζₑ + Bζₑ)*(1 - exp(-dt/τ)) + sζ*exp(-dt/τ)
    # sζ .+= @. (Aζₑ - Bζₑ - (Aζₑ + Bζₑ)*sζ)*dt
    # E
    τ  .= @. 1 / 2Aηₑ
    sη .= @. sη*exp(-dt/τ)
    # sη .+= @. (-2Aηₑ * sη)*dt
    # k
    τ  .= @. 1 / 2Aκₑ
    sκ .= @. sκ*exp(-dt/τ)
    # sκ .+= @. (-2Aκₑ * sκ)*dt
    if any(qed)
        part.s[:, qed] .= sζ[qed]'.*ζ[:, qed] .+ sη[qed]'.*η[:, qed] .+ sκ[qed]'.*κ[:, qed]
    end
    return -part.p .*  δ
end

function createphotons(r::Array{Float64,2}, p::Array{Float64,2})
    eff = any(p .!= 0, dims=1)[:]
    return Photons(
        r = r[:, eff],
        p = p[:, eff],
    )
end
createphotons(xp::Nothing, t::Float64) = nothing
