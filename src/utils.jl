export cross, dot, Particles, EM


import LinearAlgebra.cross
@inline function cross(a::Array{Float64,2}, b::Array{Float64,2})
    if size(a) != size(b)
        error("a != b")
    end
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end

    c = zeros(size(a))
    @views @. c[1, :] .= a[2, :] .* b[3, :] - a[3, :] .* b[2, :]
    @views @. c[2, :] .= a[3, :] .* b[1, :] - a[1, :] .* b[3, :]
    @views @. c[3, :] .= a[1, :] .* b[2, :] - a[2, :] .* b[1, :]
    return c
end

@inline function cross(a::Vector{Float64}, b::Array{Float64,2})
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end
    c = zeros(size(b))
    @views @. c[1, :] .= a[2] .* b[3, :] - a[3] .* b[2, :]
    @views @. c[2, :] .= a[3] .* b[1, :] - a[1] .* b[3, :]
    @views @. c[3, :] .= a[1] .* b[2, :] - a[2] .* b[1, :]

    return c

end

import LinearAlgebra.dot
@inline function dot(a::Array{Float64,2}, b::Array{Float64,2})
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end
    return sum(a .* b, dims = 1)
end
@inline function dot(a::Vector{Float64}, b::Array{Float64,2})
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end
    return sum(a .* b, dims = 1)
end

struct Particles
    id::Int
    r::Array{Float64,2}
    p::Array{Float64,2}
    s::Array{Float64,2}
    npart::Int
    γ::Array{Float64,2}
    v::Array{Float64,2}
    χ::Array{Float64,2}
    optical_depth::Array{Float64,2}
    nt::Int
    dt::Float64

    function Particles(;id::Int, r::Array{Float64,2}, p::Array{Float64,2}, s::Array{Float64,2}, nt::Int, dt::Float64)
        npart = size(r, 2)
        γ = sqrt.(p ⋅ p ./ (m^2 * c²) .+ 1)
        v = p ./ m ./ γ
        χ = zeros(1, npart) # not initialized according to field
        optical_depth = -log.(1 .- rand(1, npart))

        return new(id, r, p, s, npart, γ, v, χ, optical_depth, nt, dt)
    end
end
get_gamma(part::Particles) = sqrt.(part.p ⋅ part.p ./ (m^2 * c²) .+ 1)
get_vel(part::Particles) = part.p ./ m ./ get_gamma(part)

struct Photons
    r::Array{Float64,2}
    p::Array{Float64,2}

    function Photons(;r::Array{Float64,2}, p::Array{Float64,2})
        return new(r, p)
    end
end
struct EM
    E::Array{Float64,2}
    B::Array{Float64,2}
    function EM(sz::Int)
        return new(zeros(3, sz), zeros(3, sz))
    end
end

function get_chi(part::Particles, em::EM)
    γ = part.γ
    p = part.p

    E = em.E
    B = em.B

    return e*ħ/(m^3*c^4) * sqrt.(sum((γ.*m*c.*E + p×B*c).^2, dims=1) .- (p⋅E).^2)
end

mutable struct waitbar
    N::Int
    tic0::Float64
    tic::Float64
    function waitbar(N::Int)
        println("start.")
        new(N, time(), time())
    end
end
function (wb::waitbar)(n::Int)
    N = wb.N
    tic0 = wb.tic0
    tic = copy(wb.tic)
    toc = time()

    if mod(n, div(N, 10)) == 0
        @printf(
            "%3d%%, %5.1fs elapsed, %5.1fs to go, %4.0fms per step.\n",
            div(n*100, N), toc-tic0, (toc-tic)/N*10 * (N-n), (toc-tic)/N*10*1E3
        )
        wb.tic = toc
    end
    if n == N
        println("done")
    end
end
